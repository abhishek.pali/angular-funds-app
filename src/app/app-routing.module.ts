import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ExploreFundsComponent } from './components/explore-funds/explore-funds.component';
import { FundComponent } from './components/fund/fund.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'explore', component: ExploreFundsComponent},
  {path: 'explore/:code', component: FundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
