import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Funds } from '../models/Funds';

@Injectable({
  providedIn: 'root'
})
export class FundsService {

  fundsUrl: string = 'https://api.kuvera.in/api/v3/funds.json';
  fundInfoUrl: string = 'https://api.kuvera.in/api/v3/funds/';

  constructor(private http: HttpClient) { }

  getFunds(): Observable<Funds[]> {
    return this.http.get<Funds[]>(`${this.fundsUrl}`);
  }

  getFundInfo(path): Observable<any> {
    return this.http.get(`${this.fundInfoUrl}${path}.json`);
  }
}
