export class Funds {
    code: string;
    name: string;
    category: string;
    reinvestment: string;
    fund_house: string;
    fund_type: string;
    fund_category: string;
    plan: string;
    returns: object;
    volatility: number;
}