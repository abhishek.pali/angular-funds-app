import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FundsTableComponent } from '../funds-table/funds-table.component';

@Component({
  selector: 'app-explore-funds',
  templateUrl: './explore-funds.component.html',
  styleUrls: ['./explore-funds.component.css']
})
export class ExploreFundsComponent implements OnInit, AfterViewInit {

  @ViewChild(FundsTableComponent) childReference: FundsTableComponent;

  constructor() {
   }

  filterTypeArr: string[] = ["Fund Category", "Fund Type", "Plan"];
  firstFilterTypeArr: string[] = [];
  secondFilterTypeArr: string[] = [];
  thirdFilterTypeArr: string[] = [];
  firstFilterOption: string = "";
  secondFilterOption: string = "";
  thirdFilterOption: string = "";
  firstFilter: string = "";
  secondFilter: string = "";
  thirdFilter: string = "";
  None: string = "None";
  firstSubFilterTypeArr: string[] = [];
  secondSubFilterTypeArr: string[] = [];
  thirdSubFilterTypeArr: string[] = [];
  firstSubFilter: string = "";
  secondSubFilter: string = "";
  thirdSubFilter: string = "";
  filterApplied: boolean = false;

  ngOnInit(): void {
    this.firstFilterOption = "None";
    this.secondFilterOption = "None";
    this.thirdFilterOption = "None";
    this.firstFilterTypeArr = this.filterTypeArr;
    this.secondFilterTypeArr = this.filterTypeArr;
    this.thirdFilterTypeArr = this.filterTypeArr;
  }

  ngAfterViewInit() {
  }

  addFirstSubType(event: any) {

    this.firstFilterOption = event.target.value;
    this.secondFilterTypeArr = this.secondFilterTypeArr.filter(filterType => filterType !== this.firstFilterOption);
    this.thirdFilterTypeArr = this.thirdFilterTypeArr.filter(filterType => filterType !== this.firstFilterOption);
    if (this.firstFilterOption === "Fund Category")
      { 
        this.firstFilter = "fund_category";
        this.firstSubFilterTypeArr = this.childReference.fundCategoryArr;
      }
    
    if (this.firstFilterOption === "Fund Type")
      {
        this.firstFilter = "fund_type";
        this.firstSubFilterTypeArr = this.childReference.fundTypeArr;
      }

    if (this.firstFilterOption === "Plan")
      {
        this.firstFilter = "plan";
        this.firstSubFilterTypeArr = this.childReference.planArr;
      }

    if (this.firstFilterOption === "None")
      {
        this.firstFilter = "";
        this.firstSubFilterTypeArr = [];
      }
  }

  addSecondSubType(event: any) {

    this.secondFilterOption = event.target.value;
    this.firstFilterTypeArr = this.firstFilterTypeArr.filter(filterType => filterType !== this.secondFilterOption);
    this.thirdFilterTypeArr = this.thirdFilterTypeArr.filter(filterType => filterType !== this.secondFilterOption);
    if (this.secondFilterOption === "Fund Category")
      {
        this.secondFilter = "fund_category";
        this.secondSubFilterTypeArr = this.childReference.fundCategoryArr;
      }
    
    if (this.secondFilterOption === "Fund Type")
      {
        this.secondFilter = "fund_type";
        this.secondSubFilterTypeArr = this.childReference.fundTypeArr;
      }

    if (this.secondFilterOption === "Plan")
      {
        this.secondFilter = "plan";
        this.secondSubFilterTypeArr = this.childReference.planArr;
      }

    if (this.secondFilterOption === "None")
      {
        this.secondFilter = "";
        this.secondSubFilterTypeArr = [];
      }
  }

  addThirdSubType(event: any) {

    this.thirdFilterOption = event.target.value;
    this.firstFilterTypeArr = this.firstFilterTypeArr.filter(filterType => filterType !== this.thirdFilterOption);
    this.secondFilterTypeArr = this.secondFilterTypeArr.filter(filterType => filterType !== this.thirdFilterOption);
      if (this.thirdFilterOption === "Fund Category")
        {
          this.thirdFilter = "fund_category";
          this.thirdSubFilterTypeArr = this.childReference.fundCategoryArr;
        }
        
      if (this.thirdFilterOption === "Fund Type")
        {
          this.thirdFilter = "fund_type";
          this.thirdSubFilterTypeArr = this.childReference.fundTypeArr;
        }
    
      if (this.thirdFilterOption === "Plan")
        {
          this.thirdFilter = "plan";
          this.thirdSubFilterTypeArr = this.childReference.planArr;
        }

      if (this.thirdFilterOption === "None")
        {
          this.thirdFilter = "";
          this.thirdSubFilterTypeArr = [];
        }
    }

  addFirstSubFilter(event: any)
    {
      this.firstSubFilter = event.target.value;
    }

  addSecondSubFilter(event: any)
    {
      this.secondSubFilter = event.target.value;
    }

  addThirdSubFilter(event: any)
    {
      this.thirdSubFilter = event.target.value;
    }

  resetFilter() {
      console.log("hi");
      this.firstFilterOption = "None";
      this.secondFilterOption = "None";
      this.thirdFilterOption = "None";
      this.firstFilter = "";
      this.secondFilter = "";
      this.thirdFilter = "";
      this.firstSubFilter = "";
      this.secondSubFilter = "";
      this.thirdSubFilter = "";
      this.firstFilterTypeArr = [];
      this.secondFilterTypeArr = [];
      this.thirdFilterTypeArr = [];
      this.firstSubFilterTypeArr = [];
      this.secondSubFilterTypeArr = [];
      this.thirdSubFilterTypeArr = [];
      this.filterApplied = false;
      window.location.reload();
    }
}

