import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExploreFundsComponent } from './explore-funds.component';

describe('ExploreFundsComponent', () => {
  let component: ExploreFundsComponent;
  let fixture: ComponentFixture<ExploreFundsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExploreFundsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExploreFundsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
