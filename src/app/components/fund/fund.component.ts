import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FundsService } from '../../services/funds.service';

@Component({
  selector: 'app-fund',
  templateUrl: './fund.component.html',
  styleUrls: ['./fund.component.css'],
  providers: [Location]
})
export class FundComponent implements OnInit {

  path: string;
  fundInfo: any[];

  constructor(private location: Location, private fundsService: FundsService) { 
    this.path = this.location.path().slice(9);
  }

  ngOnInit(): void {
    this.fundsService.getFundInfo(this.path).subscribe(info => {
      this.fundInfo = info;
    })
  }

  getPath() {
    console.log(this.location.path());  
  }

}
