import { Component, OnInit, Input } from '@angular/core';
import { FundsService } from '../../services/funds.service';
import { Funds } from '../../models/Funds';

@Component({
  selector: 'app-funds-table',
  templateUrl: './funds-table.component.html',
  styleUrls: ['./funds-table.component.css']
})
export class FundsTableComponent implements OnInit {

  @Input() firstFilter: string;  
  @Input() secondFilter: string;
  @Input() thirdFilter: string;
  @Input() firstSubFilter: string;  
  @Input() secondSubFilter: string;
  @Input() thirdSubFilter: string;
  @Input() filterApplied: boolean;

  tableHeaders: string[] = ["Name of Scheme", "Fund Category", "Fund Type", "Plan", "Year 1 Returns", "Year 3 Returns"];
  tableIndex: string[] = ["name", "fund_category", "fund_type", "plan"];
  funds: Funds[];
  fundCategoryArr: string[];
  fundTypeArr: string[];
  planArr: string[];
  sortingApplied: boolean = false;

  constructor(private fundsService: FundsService) { 
  }

  ngOnInit(): void {
      this.fundsService.getFunds().subscribe(funds => {
        this.funds = funds;

      this.fundCategoryArr = funds.reduce(function(arr, fund) {
        if (fund.fund_category !== undefined && fund.fund_category !== null)
          {
            if(arr.indexOf(fund.fund_category) === -1)
              {
                arr.push(fund.fund_category);
              }
          }
        return arr;
      }, []);
    
      this.fundTypeArr = funds.reduce(function(arr, fund) {
        if (fund.fund_type !== undefined && fund.fund_type !== null)
          {
            if(arr.indexOf(fund.fund_type) === -1)
              {
                arr.push(fund.fund_type);
              }
          }
        return arr;
      }, []);
    
      this.planArr = funds.reduce(function(arr, fund) {
        if (fund.plan !== undefined && fund.plan !== null)
          {
            if(arr.indexOf(fund.plan) === -1)
              {
                arr.push(fund.plan);
              }
          }
        return arr;
      }, []);
    })
  }

  sortByScheme() {
    this.sortingApplied = true;
    if (this.filterApplied)
      {
        this.funds = this.funds.filter(fund => fund.name !== undefined && fund.name !== null).sort(function (first, second) {
    
          let firstName = first.name.toUpperCase();
          let secondName = second.name.toUpperCase();
          if (firstName > secondName)
            {
              return 1;
            }
          else if (firstName < secondName)
            {
              return -1;
            }
          else
            {
              return 0;
            }
          });      
      }
    else
      {
        this.fundsService.getFunds().subscribe(funds => {
          this.funds = funds.filter(fund => fund.name !== undefined && fund.name !== null).sort(function (first, second) {
        
          let firstName = first.name.toUpperCase();
          let secondName = second.name.toUpperCase();
          if (firstName > secondName)
            {
              return 1;
            }
          else if (firstName < secondName)
            {
              return -1;
            }
          else
            {
              return 0;
            }
          });
        });   
      }   
  }

  sortByFundCategory() {
    this.sortingApplied = true;
    if (this.filterApplied)
      {
        this.funds = this.funds.filter(fund => fund.fund_category !== undefined && fund.fund_category !== null).sort(function (first, second) {

              let firstFundCategory = first.fund_category.toUpperCase();
              let secondFundCategory = second.fund_category.toUpperCase();
              if (firstFundCategory > secondFundCategory)
                {
                  return 1;
                }
              else if (firstFundCategory < secondFundCategory)
                {
                  return -1;
                }
              else
                {
                  return 0;
                }
          });
      }
    else
      {
        this.fundsService.getFunds().subscribe(funds => {
          this.funds = funds.filter(fund => fund.fund_category !== undefined && fund.fund_category !== null).sort(function (first, second) {

              let firstFundCategory = first.fund_category.toUpperCase();
              let secondFundCategory = second.fund_category.toUpperCase();
              if (firstFundCategory > secondFundCategory)
                {
                  return 1;
                }
              else if (firstFundCategory < secondFundCategory)
                {
                  return -1;
                }
              else
                {
                  return 0;
                }
          });
        });    
      }
  }

  sortByFundType() {
    this.sortingApplied = true;
    if (this.filterApplied)
      {
        this.funds = this.funds.filter(fund => fund.fund_type !== undefined && fund.fund_type !== null).sort(function (first, second) {

              let firstFundType = first.fund_type.toUpperCase();
              let secondFundType = second.fund_type.toUpperCase();
              if (firstFundType > secondFundType)
                {
                  return 1;
                }
              else if (firstFundType < secondFundType)
                {
                  return -1;
                }
              else
                {
                  return 0;
                }
          });
      }
    else
      {
        this.fundsService.getFunds().subscribe(funds => {
          this.funds = funds.filter(fund => fund.fund_type !== undefined && fund.fund_type !== null).sort(function (first, second) {
    
              let firstFundType = first.fund_type.toUpperCase();
              let secondFundType = second.fund_type.toUpperCase();
              if (firstFundType > secondFundType)
                {
                  return 1;
                }
              else if (firstFundType < secondFundType)
                {
                  return -1;
                }
              else
                {
                  return 0;
                }
          });
        });    
      }
  }

  sortByPlan() {
    this.sortingApplied = true;
    if (this.filterApplied)
      {
        this.funds = this.funds.filter(fund => fund.plan !== undefined && fund.plan !== null).sort(function (first, second) {

              let firstPlan = first.plan.toUpperCase();
              let secondPlan = second.plan.toUpperCase();
              if (firstPlan > secondPlan)
                {
                  return 1;
                }
              else if (firstPlan < secondPlan)
                {
                  return -1;
                }
              else
                {
                  return 0;
                }
          });
      }
    else
      {
        this.fundsService.getFunds().subscribe(funds => {
          this.funds = funds.filter(fund => fund.plan !== undefined && fund.plan !== null).sort(function (first, second) {

              let firstPlan = first.plan.toUpperCase();
              let secondPlan = second.plan.toUpperCase();
              if (firstPlan > secondPlan)
                {
                  return 1;
                }
              else if (firstPlan < secondPlan)
                {
                  return -1;
                }
              else
                {
                  return 0;
                }
          });
        });    
      }
  }

  sortByYearOneReturns() {
    this.sortingApplied = true;
    if (this.filterApplied)
      {
        this.funds = this.funds.filter(fund => fund["returns"]["year_3"] !== undefined).sort(function (first, second) {

          if (first["returns"]["year_1"] > second["returns"]["year_1"])
            {
              return -1;
            }
          else if (first["returns"]["year_1"] < second["returns"]["year_1"])
            {
              return 1;
            }
          else
            {
              return 0;
            }
          });    
      }
    else
      {
        this.fundsService.getFunds().subscribe(funds => {
          this.funds = funds.filter(fund => fund["returns"]["year_1"] !== undefined).sort(function (first, second) {
    
          if (first["returns"]["year_1"] > second["returns"]["year_1"])
            {
              return -1;
            }
          else if (first["returns"]["year_1"] < second["returns"]["year_1"])
            {
              return 1;
            }
          else
            {
              return 0;
            }
          });
        });   
      } 
  }

  sortByYearThreeReturns() {
    this.sortingApplied = true;
    if (this.filterApplied)
      {
        this.funds = this.funds.filter(fund => fund["returns"]["year_3"] !== undefined).sort(function (first, second) {

          if (first["returns"]["year_3"] > second["returns"]["year_3"])
            {
              return -1;
            }
          else if (first["returns"]["year_3"] < second["returns"]["year_3"])
            {
              return 1;
            }
          else
            {
              return 0;
            }
          });
      }
    else
      {
        this.fundsService.getFunds().subscribe(funds => {
          this.funds = funds.filter(fund => fund["returns"]["year_3"] !== undefined).sort(function (first, second) {
              
          if (first["returns"]["year_3"] > second["returns"]["year_3"])
            {
              return -1;
            }
          else if (first["returns"]["year_3"] < second["returns"]["year_3"])
            {
              return 1;
            }
          else
            {
              return 0;
            }
          });
        }); 
      }   
  }

  filterBy() {

    this.filterApplied = true;

    if (this.firstFilter !== "")
      {
        if (this.secondFilter !== "" && this.thirdFilter !== "")
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter).filter(
                  fund => fund[this.secondFilter] === this.secondSubFilter
                ).filter(fund => fund[this.thirdFilter] === this.thirdSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter).filter(
                    fund => fund[this.secondFilter] === this.secondSubFilter
                  ).filter(fund => fund[this.thirdFilter] === this.thirdSubFilter);
                });
              }
          }
        else if (this.secondFilter === "" && this.thirdFilter !== "")
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter).filter(
                  fund => fund[this.thirdFilter] === this.thirdSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter).filter(
                    fund => fund[this.thirdFilter] === this.thirdSubFilter);
                });
              }
          }
        else if (this.secondFilter !== "" && this.thirdFilter === "")
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter).filter(
                  fund => fund[this.secondFilter] === this.secondSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter).filter(
                    fund => fund[this.secondFilter] === this.secondSubFilter);
                });
              }
          }
        else
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(fund => fund[this.firstFilter] === this.firstSubFilter);
                });
              }
          }
      }
    else
      {
        if (this.secondFilter !== "" && this.thirdFilter !== "")
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(
                  fund => fund[this.secondFilter] === this.secondSubFilter
                ).filter(fund => fund[this.thirdFilter] === this.thirdSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(
                    fund => fund[this.secondFilter] === this.secondSubFilter
                  ).filter(fund => fund[this.thirdFilter] === this.thirdSubFilter);
                });
              }
          }
        else if (this.secondFilter === "" && this.thirdFilter !== "")
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(
                  fund => fund[this.thirdFilter] === this.thirdSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(
                    fund => fund[this.thirdFilter] === this.thirdSubFilter);
                });
              }
          }
        else if (this.secondFilter !== "" && this.thirdFilter === "")
          {
            if (this.sortingApplied)
              {
                this.funds = this.funds.filter(
                  fund => fund[this.secondFilter] === this.secondSubFilter);
              }
            else
              {
                this.fundsService.getFunds().subscribe(funds => {
                  this.funds = funds.filter(
                    fund => fund[this.secondFilter] === this.secondSubFilter);
                });
              }
          }
        else
          {
            
          }
      }
  }
}